from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# @param request : to capture all the request that are sent to our web


def index(request):
    return HttpResponse("Hello, world. You're at the application index.")
